﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FitnessClicker
{
    public static class Methods
    {
        /// <summary>
        /// Чтобы было больше тестов
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static int PlussAction(int exerciseCounter)
        {
            return exerciseCounter += 1;
        }

        public static int MinusAction(int exerciseCounter)
        {
            return exerciseCounter -= 1;
        }

        /// <summary>
        /// Метод приобразует, приходящее ему double для использования в изменении цвета, в байты (0 - 255!!!)
        /// Использовать в ползунках значения от 0 до 255 и шаг в 1.
        /// </summary>
        /// <param name="sliderValue"></param>
        /// <returns>значение цвета в байтах. Да, от 0 до 255 </returns>
        public static byte PrepareValueForColor(double sliderValue)
        {
            if (sliderValue >= 0.0 && sliderValue <= 255.0)
            {
                return byte.Parse(Math.Round(sliderValue).ToString());
            }
            else
            {
                return 0;
            }
        }
    }
}
