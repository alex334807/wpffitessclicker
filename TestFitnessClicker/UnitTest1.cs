﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FitnessClicker;

namespace TestFitnessClicker
{
    [TestClass]
    public class UnitTest1
    {

        [TestMethod]
        public void PlusAction_Test()
        {
            var a = 0;
            var b = 1;
            var c = 2;
            
            var result = Methods.PlussAction(a);
            var result1 = Methods.PlussAction(b);
            var result2 = Methods.PlussAction(c);
            
            Assert.AreEqual(1, result);
            Assert.AreEqual(2, result1);
            Assert.AreEqual(3, result2);
            
        }

        [TestMethod]
        public void PlusAction_Test_NegativeNumber()
        {
            var a = -1;

            var result = Methods.PlussAction(a);

            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void MinusAction_Test()
        {
            var a = 1;
            var b = -1;

            var result = Methods.MinusAction(a);
            var result1 = Methods.MinusAction(b);

            Assert.AreEqual(0, result);
            Assert.AreEqual(-2, result1);
        }

        [TestMethod]
        public void PrepareValueForColor_Test_Type()
        {
            double a = 0;
            double b = 255;

            var result = Methods.PrepareValueForColor(a);
            var result1 = Methods.PrepareValueForColor(b);

            Assert.AreEqual(TypeCode.Byte, result.GetTypeCode() );
            Assert.AreEqual(TypeCode.Byte, result1.GetTypeCode() );
        }

        [TestMethod]
        public void PrepareValueForColor_Test_MaxMin()
        {
            double a = -1;
            double b = 256;

            var result = Methods.PrepareValueForColor(a);
            var result1 = Methods.PrepareValueForColor(b);

            Assert.AreEqual(0, result);
            Assert.AreEqual(0, result1);
        }

        [TestMethod]
        public void PrepareValueForColor_Test_NotInteger()
        {
            double a = 0.50000000;
            double b = 254.50000001;

            var result = Methods.PrepareValueForColor(a);
            var result1 = Methods.PrepareValueForColor(b);

            Assert.AreEqual(0, result);
            Assert.AreEqual(255, result1);
        }
    }
}
