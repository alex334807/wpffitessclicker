﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;

using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;

namespace FitnessClicker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public System.Windows.Threading.DispatcherTimer timer = new System.Windows.Threading.DispatcherTimer();
        

        private SoundPlayer soundP = new SoundPlayer();
        public int exerciseCounter = 0;
        private int hours = 0;
        private int minutes = 0;
        private int seconds = 0;
        //private int milliseconds = 0;
        private bool isWiden;

        public MainWindow()
        {
            soundP.SoundLocation = "button-19.wav";
            soundP.Load();
            InitializeComponent();
            CounterText.Text = exerciseCounter.ToString();
            timer.Tick += new EventHandler(CheckTime);
            timer.Interval = new TimeSpan(0, 0, 1);

        }

        private void Button_Plus_Click(object sender, RoutedEventArgs e)
        {
            soundP.Play();
            ///Заморавчиваюсь, чтобы была возможность тестировать
            exerciseCounter = Methods.PlussAction(exerciseCounter);
            CounterText.Text = exerciseCounter.ToString();
        }

        private void Button_Minus_Click(object sender, RoutedEventArgs e)
        {
            soundP.Play();
            exerciseCounter = Methods.MinusAction(exerciseCounter); ;
            CounterText.Text = exerciseCounter.ToString();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Button_Exit_Click(object sender, RoutedEventArgs e)
        {
            soundP.Play();
            Environment.Exit(0);
        }

        private void Button_Start_Click(object sender, RoutedEventArgs e)
        {
            timer.Start();
            //timersOn = true;
            //InitializeTimer();
        }

        private void Button_Stop_Click(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            hours = 0;
            minutes = 0;
            seconds = 0;
            TimerText12.Text = "0 : 0 : 0";
        }

        private void Button_Pause_Click(object sender, RoutedEventArgs e)
        {
            timer.Stop();
            //timer.Tick += null;
        }

        /// <summary>
        /// На графике переключает Цифры
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckTime(object sender, EventArgs e)
        {
            TimerText12.Text = hours + " : " + minutes + " : " + seconds;
            seconds += 1;

            if (seconds >= 60)
            {
                seconds = 0;
                minutes += 1;

                if (minutes >= 60)
                {
                    minutes = 0;
                    hours += 1;

                    if (hours >= 24)
                    {
                        timer.Stop();
                    }
                }
            }
        }

        private void Button_Settings_Click(object sender, RoutedEventArgs e)
        {
            soundP.Play();
            if (SettingsGrid.Visibility == Visibility.Hidden)
            {
                SettingsGrid.Visibility = Visibility.Visible;
            }
            else
            {
                SettingsGrid.Visibility = Visibility.Hidden;
            }

        }

        private void ResizeAction(object sender, RoutedEventArgs e)
        {
            TopTitle.Width = MainWindow1.Width;
            MainGrid.Width = MainWindow1.Width;
            MainGrid.Height = MainWindow1.Height;
        }

        private void TransparentOn(object sender, RoutedEventArgs e)
        {
            MainWindow1.Opacity = TransparentSlider.Value;
            TransparentSlider.IsEnabled = true;
        }

        private void TransparentOff(object sender, RoutedEventArgs e)
        {
            MainWindow1.Opacity = 1;
            TransparentSlider.IsEnabled = false;
        }

        private void TransparentSliderChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (TransparentOption.IsChecked == true)
            {
                MainWindow1.Opacity = TransparentSlider.Value;
            }
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isWiden = true;
        }

        private void Rectangle_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isWiden = false;
            Rectangle rect = (Rectangle)sender;
            rect.ReleaseMouseCapture();
        }

        private void Rectangle_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            if (isWiden)
            {
                rect.CaptureMouse();
                double newWidth = e.GetPosition(this).X;
                if (newWidth > 0) this.Width = newWidth;
            }
        }

        private void DownRectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isWiden = true;
        }

        private void DownRectangle_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isWiden = false;
            Rectangle rect = (Rectangle)sender;
            rect.ReleaseMouseCapture();
        }

        private void DownRectangle_MouseMove(object sender, MouseEventArgs e)
        {
            Rectangle rect = (Rectangle)sender;
            if (isWiden)
            {
                rect.CaptureMouse();
                double newHeight = e.GetPosition(this).Y + 5;
                if (newHeight > 0) this.Height = newHeight;
            }
        }

        private void SecondColor_Slider_Changed(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if ( SliderMainColorGreen != null && SliderMainColorBlue != null && SliderMainColorRed != null )
            {
                ChangeThemeColor( Color.FromRgb(
                    Methods.PrepareValueForColor(SliderSecondColorRed.Value),
                    Methods.PrepareValueForColor(SliderSecondColorGreen.Value),
                    Methods.PrepareValueForColor(SliderSecondColorBlue.Value)),
                    Color.FromRgb(
                    Methods.PrepareValueForColor(SliderMainColorRed.Value),
                    Methods.PrepareValueForColor(SliderMainColorGreen.Value),
                    Methods.PrepareValueForColor(SliderMainColorBlue.Value)));

            }
        }

        private void ChangeThemeColor(Color secondColor, Color mainColor)
        {
            SettingsGrid.Background = new LinearGradientBrush(secondColor, mainColor, 90.0);
            TopTitle.Background = new SolidColorBrush(secondColor);
            //TimerBackGrid.Background = new SolidColorBrush(Color.FromRgb(secondColor.R, secondColor.G, secondColor.B));
            MainGrid.Background = new SolidColorBrush(mainColor);
        }

        private void Button_Clear_Click(object sender, RoutedEventArgs e)
        {
            exerciseCounter = 0;
            CounterText.Text = "0";
        }
    }
}
